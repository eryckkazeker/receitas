# Receitas

  

## O Projeto

  

Esse projeto consiste de uma API e de um front-end que consome alguns de seus endpoints

  

## API

  

A API contém os seguintes endpoints:


* POST /receita (inicializa uma receita)
* GET /receitas (devolve todas as receitas em memória)
* GET /receitas/{id} (devolve uma receita em memória por id)
* GET /receitas/{id}/ingredientes (devolve os ingredientes de uma receita)
* GET /receitas/ingredientes/{id} (devolve receitas que contenham o ingrediente definido por id)
* GET /receitas/ingredientes (devolve todos os ingredientes utilizados em receitas)
* GET /ingredientes (devolve os ingredientes disponíveis, ordenados alfabeticamente)

  

### POST /receita

Endpoint para cadastrar uma nova receita.

#### Entrada:

JSON com a seguinte formatação:

    {
    "_id": 0,
    "_nome": "Nome da Receita",
    "_porcoes": 1,
    "_calorias": 1,
    "_modoDePreparo": "Modo de Preparar a Receita",
    "_ingredientes": [
        {
            "_id": 3,
            "_nome": "Nome do Ingrediente"
        },
		{
			"_id": 9,
			"_nome": "Nome de outro Ingrediente"
		}]
	}

#### Saída:
O id do objeto cadastrado em formato String: "123"

### GET /receitas
Seleciona todas as receitas cadastradas.

#### Saída:
JSON com um array  de objetos Receita.

    [
    {
        "Id": 1,
        "Nome": "Receita1",
        "Porcoes": 1,
        "Calorias": 1,
        "Ingredientes": [
            {
                "Id": 3,
                "Nome": "Agua"
            },
            {
                "Id": 6,
                "Nome": "Oleo"
            }
        ],
        "ModoDePreparo": "Modo de preparo"
    },
    {
        "Id": 2,
        "Nome": "Receita2",
        "Porcoes": 1,
        "Calorias": 2,
        "Ingredientes": [
            {
                "Id": 1,
                "Nome": "Arroz"
            }
        ],
        "ModoDePreparo": "Modo de Preparo"
    },
	...
	]
	
### GET /receitas/{id}
Retorna a Receita correspondente ao id

#### Saída:

    {
        "Id": 2,
        "Nome": "Receita2",
        "Porcoes": 1,
        "Calorias": 2,
        "Ingredientes": [
            {
                "Id": 1,
                "Nome": "Arroz"
            }
        ],
        "ModoDePreparo": "Modo de Preparo"
    }

### GET /receitas/{id}/ingredientes
Retorna a lista de ingredientes utilizados na receita correspondente ao id.

#### Saída:

        [
            {
                "Id": 3,
                "Nome": "Agua"
            },
            {
                "Id": 6,
                "Nome": "Oleo"
            }
        ]

### GET /receitas/ingredientes/{id}
Retorna todas as receitas que contêm o ingrediente correspondente ao id.

### GET /receitas/ingredientes
Retorna todos os ingredientes que são utilizados em qualquer receita.

### GET /ingredientes
Retorna uma lista com todos os  ingredientes disponíveis em ordem alfabética.

#### Saída:

    [
    {
        "Id": 3,
        "Nome": "Agua"
    },
    {
        "Id": 1,
        "Nome": "Arroz"
    },
    {
        "Id": 4,
        "Nome": "Farinha"
    },
    {
        "Id": 2,
        "Nome": "Feijao"
    },
    {
        "Id": 6,
        "Nome": "Oleo"
    },
    {
        "Id": 5,
        "Nome": "Ovo"
    }
	]

## Host da API
A api está hospedada no serviço de Cloud Azure.
O endereço é: http://receitasapieryck.azurewebsites.net/api

## Front-end
O front-end foi inteiramente desenvolvido em HTML/CSS/Javascript

Testado nos seguintes navegadores:

* Chrome Versão 70.0.3538.110
* Edge Versão 42.17134.1.0
	