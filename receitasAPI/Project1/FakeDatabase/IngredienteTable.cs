﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using receitas.Models;

namespace receitas.FakeDatabase
{
    public class IngredienteTable
    {
        public List<Ingrediente> ListaIngredientes;
        public static IngredienteTable _instance = null;

        private IngredienteTable()
        {
            ListaIngredientes = new List<Ingrediente>();
        }

        public static IngredienteTable Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new IngredienteTable();
                }

                return _instance;
            }
        }

        /// <summary>
        /// Retorna todos os ingredientes cadastrados por
        /// ordem alfabetica de nome
        /// </summary>
        /// <returns></returns>
        public List<Ingrediente> Get()
        {
            return ListaIngredientes.OrderBy(ingrediente => ingrediente.Nome).ToList();
        }

        public Ingrediente Get(int id)
        {
            return (Ingrediente)ListaIngredientes.Where(ingrediente => ingrediente.Id == id).ToList()[0];
        }

        public void Insert(Ingrediente ingrediente)
        {
            ListaIngredientes.Add(ingrediente);
        }

        public void Clear()
        {
            ListaIngredientes.Clear();
        }
    }
}
