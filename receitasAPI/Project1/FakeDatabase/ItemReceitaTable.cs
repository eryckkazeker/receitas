﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using receitas.Models;

namespace receitas.FakeDatabase
{
    public class ItemReceitaTable
    {
        public List<ItemReceita> ListaItemReceita;
        public static ItemReceitaTable _instance = null;

        private ItemReceitaTable()
        {
            ListaItemReceita = new List<ItemReceita>();
        }

        public static ItemReceitaTable Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ItemReceitaTable();
                }

                return _instance;
            }
        }

        public List<Ingrediente> GetIngredientesDeReceitaPorId(int id)
        {

            List<ItemReceita> listaIngredientesReceita = ListaItemReceita
                                                    .Where(itemReceita => itemReceita.Receita.Id == id)
                                                    .ToList();
            List<Ingrediente> listaRetorno = new List<Ingrediente>();
            
            foreach (ItemReceita item in listaIngredientesReceita)
            {
                Ingrediente ing = IngredienteTable.Instance.Get(item.Ingrediente.Id);
                listaRetorno.Add(ing);
            }
            return listaRetorno;
        }

        public List<Ingrediente> GetIngredientesUtilizados()
        {
            List<Ingrediente> listaRetorno = new List<Ingrediente>();

            List<int> listaIdIngredientes = new List<int>();
            
            foreach(ItemReceita item in ListaItemReceita)
            {
                listaIdIngredientes.Add(item.Ingrediente.Id);
            }

            var listaIdsUnicos = listaIdIngredientes.Distinct().ToList();

            foreach(int id in listaIdsUnicos)
            {
                listaRetorno.Add(IngredienteTable.Instance.Get(id));
            }

            return listaRetorno;


        }

        public List<Receita> GetReceitasCom(Ingrediente ingrediente)
        {
            List<ItemReceita> listaReceitasComIngrediente = ListaItemReceita
                                                    .Where(itemReceita => itemReceita.Ingrediente.Id == ingrediente.Id)
                                                    .ToList();
            

            List<Receita> listaRetorno = new List<Receita>();
            
            foreach(ItemReceita item in listaReceitasComIngrediente)
            {
                Receita rec = ReceitaTable.Instance.Get(item.Receita.Id);
                listaRetorno.Add(rec);
            }
            return listaRetorno;
        }

        public void Insert(ItemReceita item)
        {
            ListaItemReceita.Add(item);
        }

        public void Clear()
        {
            ListaItemReceita.Clear();
        }
    }
}
