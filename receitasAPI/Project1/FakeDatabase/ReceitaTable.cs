﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using receitas.Models;

namespace receitas.FakeDatabase
{
    public class ReceitaTable
    {

        public List<Receita> ListaReceitas;
        public int LastIndex { get; private set; }
        public static ReceitaTable _instance = null;

        private ReceitaTable()
        {
            ListaReceitas = new List<Receita>();
            LastIndex = 0;
        }

        public static ReceitaTable Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReceitaTable();
                }

                return _instance;
            }
        }
        
        public List<Receita> Get()
        {
            return ListaReceitas;
        }

        public Receita Get(int id)
        {
            List<Receita> listaReceitas = ListaReceitas.Where(receita => receita.Id == id).ToList();

            if (listaReceitas == null) return null;
            if (listaReceitas.Count == 0) return null;

            return listaReceitas[0];
        }

        public void Insert(Receita receita)
        {
            ListaReceitas.Add(receita);
            foreach (Ingrediente ing in receita.Ingredientes)
            {
                ItemReceitaTable.Instance.Insert(new ItemReceita(ing, receita));
            }
            LastIndex++;
        }

        public void Clear()
        {
            ListaReceitas.Clear();
            LastIndex = 0;
        }
    }
}
