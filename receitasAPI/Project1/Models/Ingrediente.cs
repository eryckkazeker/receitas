﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace receitas.Models
{
    public class Ingrediente
    {
        public int Id { get; private set; }
        public String Nome { get; private set; }

        public Ingrediente(int id, String nome)
        {
            this.Id = id;
            Nome = nome;
        }
    }
}
