﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace receitas.Models
{
    public class ItemReceita
    {
        public Ingrediente Ingrediente;
        public Receita Receita;

        public ItemReceita(Ingrediente ingrediente, Receita receita)
        {
            Ingrediente = ingrediente;
            Receita = receita;
        }
    }
}
