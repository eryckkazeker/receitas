﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using receitas.FakeDatabase;

namespace receitas.Models
{
    
    public class Receita
    {
        public int Id { get; private set; }
        public String Nome { get; private set; }
        public int Porcoes { get; private set; }
        public int Calorias { get; private set; }
        public List<Ingrediente> Ingredientes { get; private set; }
        public String ModoDePreparo { get; private set; }

        public Receita(String nome, int porcoes, int calorias, List<Ingrediente> ingredientes, String modoDePreparo)
        {
            this.Id = ReceitaTable.Instance.LastIndex + 1;
            this.Nome = nome;
            this.Porcoes = porcoes;
            this.Calorias = calorias;
            this.Ingredientes = ingredientes;
            this.ModoDePreparo = modoDePreparo;
        }

        public void AdicionaIngrediente(Ingrediente ingrediente)
        {
            Ingredientes.Add(ingrediente);
        }


    }
}
