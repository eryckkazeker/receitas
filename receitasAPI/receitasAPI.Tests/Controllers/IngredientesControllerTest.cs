﻿using System;
using NUnit.Framework;
using receitasAPI.Controllers;
using receitas.FakeDatabase;
using receitas.Models;
using System.Collections.Generic;
using System.Web.Http.Results;

namespace receitasAPI.Tests.Controllers
{
    [TestFixture]
    public class IngredientesControllerTest
    {
        private IngredientesController controller;
        [SetUp]
        public void Setup()
        {
            controller = new IngredientesController();
        }

        [TearDown]
        public void TearDown()
        {
            ReceitaTable.Instance.Clear();
            IngredienteTable.Instance.Clear();
            ItemReceitaTable.Instance.Clear();
        }

        private void SetUpGetIngredientesReceita()
        {
            TearDown();

            Ingrediente ingrediente1 = new Ingrediente(1, "AIngrediente1");
            Ingrediente ingrediente2 = new Ingrediente(2, "CIngrediente2");
            Ingrediente ingrediente3 = new Ingrediente(3, "BIngrediente3");
            Ingrediente ing4 = new Ingrediente(4, "BBBB");

            
            IngredienteTable.Instance.Insert(ingrediente1);
            IngredienteTable.Instance.Insert(ingrediente2);
            IngredienteTable.Instance.Insert(ingrediente3);
            IngredienteTable.Instance.Insert(ing4);


            Receita receita1 = new Receita("Receita1", 1, 1, new List<Ingrediente>() { ingrediente1, ingrediente2 }, "modo1");
            ReceitaTable.Instance.Insert(receita1);
            Receita receita2 = new Receita("Receita2", 2, 2, new List<Ingrediente>() { ingrediente1, ingrediente3 }, "modo2");
            ReceitaTable.Instance.Insert(receita2);
            Receita receita3 = new Receita("Receita3", 3, 3, new List<Ingrediente>() { ingrediente3, ingrediente2 }, "modo3");
            ReceitaTable.Instance.Insert(receita3);
        }

        /***************************************************************************
        * GET /receitas/{id}/ingredientes (devolve os ingredientes de uma receita) *
        ***************************************************************************/

        //Condição Normal:
        //Retorna lista de ingredientes de uma receita cadastrada
        [Test]
        public void GetIngredientesDaReceita_AllOK_RetornaListaDeIngredientes()
        {
            SetUpGetIngredientesReceita();

            var result = (OkNegotiatedContentResult<List<Ingrediente>>)controller.GetIngredientesDaReceita(1);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Ingrediente>>), result);
            Assert.AreEqual(2, result.Content.Count);
            Assert.AreEqual(1, result.Content[0].Id);
            Assert.AreEqual(2, result.Content[1].Id);
        }

        //Se não existe receita com ID informado
        //Retorna 404 - Not Found
        [Test]
        public void GetIngredientesDaReceita_IdInexistente_RetornaNotFound()
        {
            SetUpGetIngredientesReceita();

            var result = controller.GetIngredientesDaReceita(99);

            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }

        //Se id invalido
        //Retorna bad request
        [Test]
        public void GetIngredientesDaReceita_IdInvalido_RetornaBadRequest()
        {
            SetUpGetIngredientesReceita();

            var result = (BadRequestErrorMessageResult)controller.GetIngredientesDaReceita(-1);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.AreEqual("Id invalido", result.Message);
        }

        /*************************************************************************************
        * GET /ingredientes (devolve os ingredientes disponíveis, ordenados alfabeticamente) *
        *************************************************************************************/

        //Condição normal
        //Retorna ordem alfabetica
        [Test]
        public void Get_AllOK_RetornaListaEmOrdemAlfabetica()
        {
            SetUpGetIngredientesReceita();

            var result = (OkNegotiatedContentResult<List<Ingrediente>>)controller.Get();

            //Verifica ordem alfabetica
            var char1 = result.Content[0].Nome.Substring(0, 1);
            var char2 = result.Content[1].Nome.Substring(0, 1);
            var char3 = result.Content[2].Nome.Substring(0, 1);

            var order1 = String.Compare(char1, char2);
            var order2 = String.Compare(char2, char3);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Ingrediente>>), result);
            Assert.GreaterOrEqual(0, order1);
            Assert.GreaterOrEqual(0, order2);

        }

        //Caso lista vazia
        //Retorna 404 - Not Found
        [Test]
        public void Get_ListaVazia_RetornaNotFound()
        {
            TearDown();

            var result = controller.Get();

            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }


    }
}
