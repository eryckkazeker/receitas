﻿using System.Web.Http;
using receitasAPI;
using receitasAPI.Controllers;
using receitas.Models;
using receitas.FakeDatabase;
using System.Collections.Generic;
using NUnit.Framework;
using System.Web.Http.Results;
using receitasAPI.Models;

namespace receitasAPI.Tests.Controllers
{
    [TestFixture]
    public class ReceitasControllerTest
    {
        private ReceitasController controller;

        private void CriaReceitas()
        {
            List<Ingrediente> fakeIngredientList = new List<Ingrediente>() { new Ingrediente(1, "FakeIngredient") };
            ReceitaTable.Instance.Insert(new Receita("Arroz e Feijao", 4, 1000, fakeIngredientList, "Misture tudo numa panela"));
            ReceitaTable.Instance.Insert(new Receita("Strogonoff", 4, 2000, fakeIngredientList, "Carne com mostarda"));
            ReceitaTable.Instance.Insert(new Receita("Bolo", 20, 3000, fakeIngredientList, "Coloque no forno"));
        }

        [SetUp]
        public void Init()
        {
            controller = new ReceitasController();
        }

        [TearDown]
        public void TearDown()
        {
            ReceitaTable.Instance.Clear();
            IngredienteTable.Instance.Clear();
            ItemReceitaTable.Instance.Clear();
        }

        /********************************************************
        * GET /receitas (devolve todas as receitas em memória)  *
        ********************************************************/

        //Condição normal
        // Retorna uma lista de receitas
        [Test]
        public void GetReceitas_AllOK_RetornaLista()
        {
            CriaReceitas();
            OkNegotiatedContentResult<List<Receita>> result = (OkNegotiatedContentResult<List<Receita>>)controller.Get();

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Receita>>), result);
            Assert.AreEqual(3, result.Content.Count);

            TearDown();
        }

        //Se não existem receitas
        //Retorna 404 - Ainda não existem receitas cadastradas
        [Test]
        public void GetReceitas_NotFound_RetornaNotFound()
        {
            ReceitaTable.Instance.Clear();

            var result = controller.Get();

            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }

        /*************************************************************
        * GET /receitas/{id} (devolve uma receita em memória por id) *
        *************************************************************/

        //Condição Normal:
        //Retorna a receita com o id informado
        [Test]
        public void GetReceitaPorId_AllOK_RetornaReceita()
        {
            CriaReceitas();
            var id = ReceitaTable.Instance.Get()[0].Id;

            var result = (OkNegotiatedContentResult <Receita>)controller.GetReceita(id);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<Receita>), result);
            Assert.AreEqual(id, result.Content.Id);

        }

        //Se não existe receita com ID informado
        //Retorna 404 - Not Found
        [Test]
        public void GetReceitaPorId_NotFound_RetornaNotFound()
        {
            var result = controller.GetReceita(123);

            Assert.IsInstanceOf(typeof(NotFoundResult), result);

        }

        //Se id for negativo
        //Retorna 400 - bad request
        [Test]
        public void GetReceitaPorId_IdNegativo_RetornaBadRequest()
        {
            var result = (BadRequestErrorMessageResult)controller.GetReceita(-1);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.AreEqual("Id invalido", result.Message);
        }

        /***************************************************************************************************
        * GET /receitas/-/ingredientes/{id} (devolve receitas que contenham o ingrediente definido por id) *
        ***************************************************************************************************/
        //SetUp do Teste
        //Criar 3 receitas
        //Receita 1 - Ingredientes 1 e 2
        //Receita 2 - Ingredientes 1 e 3
        //Receita 3 - Ingredientes 2 e 3
        private void SetupGetReceitasIngredienteID()
        {
            TearDown();

            Ingrediente ingrediente1 = new Ingrediente(1, "Ingrediente1");
            Ingrediente ingrediente2 = new Ingrediente(2, "Ingrediente2");
            Ingrediente ingrediente3 = new Ingrediente(3, "Ingrediente3");

            
            IngredienteTable.Instance.Insert(ingrediente1);
            IngredienteTable.Instance.Insert(ingrediente2);
            IngredienteTable.Instance.Insert(ingrediente3);
            

            Receita receita1 = new Receita("Receita1", 1, 1, new List<Ingrediente>() { ingrediente1, ingrediente2 }, "modo1");
            ReceitaTable.Instance.Insert(receita1);
            Receita receita2 = new Receita("Receita2", 2, 2, new List<Ingrediente>() { ingrediente1, ingrediente3 }, "modo2");
            ReceitaTable.Instance.Insert(receita2);
            Receita receita3 = new Receita("Receita3", 3, 3, new List<Ingrediente>() { ingrediente3, ingrediente2 }, "modo3");
            ReceitaTable.Instance.Insert(receita3);
        }

        // SetUp do teste GetIngredientesUtilizados
        // Cria receitas sem ingredientes repetidos
        private void SetupGetIngredientesUtilizados()
        {
            TearDown();

            Ingrediente ingrediente1 = new Ingrediente(1, "Ingrediente1");
            Ingrediente ingrediente2 = new Ingrediente(2, "Ingrediente2");
            Ingrediente ingrediente3 = new Ingrediente(3, "Ingrediente3");

            
            IngredienteTable.Instance.Insert(ingrediente1);
            IngredienteTable.Instance.Insert(ingrediente2);
            IngredienteTable.Instance.Insert(ingrediente3);
            

            Receita receita1 = new Receita("Receita1", 1, 1, new List<Ingrediente>() { ingrediente1 }, "modo1");
            ReceitaTable.Instance.Insert(receita1);
            Receita receita2 = new Receita("Receita2", 2, 2, new List<Ingrediente>() { ingrediente3 }, "modo2");
            ReceitaTable.Instance.Insert(receita2);
            Receita receita3 = new Receita("Receita3", 3, 3, new List<Ingrediente>() { ingrediente2 }, "modo3");
            ReceitaTable.Instance.Insert(receita3);
        }

        // SetUp do teste de POST receita
        // Retorna uma Receita com uma lista de ingredientes
        private ReceitaAdapter SetUpPostReceita()
        {
            ReceitaAdapter receita = new ReceitaAdapter(0,"Receita Teste", 1, 2, SetUpIngredientes(), "Preparo");

            return receita;
        }

        private List<IngredienteAdapter> SetUpIngredientes()
        {
            IngredienteAdapter ing1 = new IngredienteAdapter(1, "Arroz");
            IngredienteAdapter ing2 = new IngredienteAdapter(2, "Feijao");

            return new List<IngredienteAdapter>() { ing1, ing2 };
        }

        //Condição Normal
        //Retorna uma lista de receitas com os ingredientes
        //Caso 1 Ingrediente 1
        [Test]
        public void GetReceitasComIngrediente_AllOKCaso1_RetornaReceitas1e2()
        {
            SetupGetReceitasIngredienteID();
            var result = (OkNegotiatedContentResult<List<Receita>>)controller.GetReceitasComIngrediente(1);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Receita>>), result);
            Assert.AreEqual(2, result.Content.Count);
            Assert.AreEqual(1, result.Content[0].Id);
            Assert.AreEqual(2, result.Content[1].Id);
        }

        //Condição Normal
        //Retorna uma lista de receitas com os ingredientes
        //Caso 2 Ingrediente 2
        [Test]
        public void GetReceitasComIngrediente_AllOKCaso2_RetornaReceitas1e3()
        {
            SetupGetReceitasIngredienteID();
            var result = (OkNegotiatedContentResult<List<Receita>>)controller.GetReceitasComIngrediente(2);

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Receita>>), result);
            Assert.AreEqual(2, result.Content.Count);
            Assert.AreEqual(1, result.Content[0].Id);
            Assert.AreEqual(3, result.Content[1].Id);
        }

        //Se não existe receita com ingrediente informado
        //Retorna 404 - Not Found
        [Test]
        public void GetReceitasComIngrediente_NenhumaReceitaComIngrediente_RetornaNotFound()
        {
            SetupGetReceitasIngredienteID();
            var result = controller.GetReceitasComIngrediente(99);

            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }

        //Se id for negativo
        //Retorna 400 - bad request
        [Test]
        public void GetReceitasComIngrediente_IdNegativo_RetornaBadRequest()
        {
            SetupGetReceitasIngredienteID();
            var result = (BadRequestErrorMessageResult)controller.GetReceitasComIngrediente(-1);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.AreEqual("Id invalido", result.Message);
        }


        /*****************************************
        * POST /receita (inicializa uma receita) *
        ******************************************/

        // Condição normal
        //Retorna 201 - Created (o ID da receita criada)
        [Test]
        public void Insert_OK_RetornaID()
        {
            TearDown();
            ReceitaAdapter receitaTeste = SetUpPostReceita();
            var result = (CreatedNegotiatedContentResult<string>)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(CreatedNegotiatedContentResult<string>), result);
            Assert.AreEqual("1", result.Content);
        }

        // Caso não possua nenhum ingrediente
        // Retorna 400 - Bad request "Receita deve ter ao menos 1 ingrediente"
        [Test]
        public void Insert_SemIngrediente_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"nome", 1, 2, null, "modo");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
        }

        // Caso não exista nome
        // Retorna 400 - Bad Request "Receita deve ter um nome"
        [Test]
        public void Insert_SemNome_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"", 1, 2, SetUpIngredientes(), "modo");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Receita deve ter um nome"));
        }

        // Caso nome contenha caracteres especiais
        // Retorna 400 - Bad Request "Nome deve conter apenas texto"

        // Caso nome contenha apenas números
        // Retorna 400 - Nome de receita invalido
        [Test]
        public void Insert_NomeApenasNumeros_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"213231", 1, 2, SetUpIngredientes(), "modo");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Nome de receita invalido"));
        }

        // Caso quantidade de porçoes não seja numero
        // Retorna 400 - Bad Request "Quantidade de porções inválida"

        // Caso quantidade de porções seja menor que zero
        // Retorna 400 - Bad Request "Quantidade de porções inválida"
        [Test]
        public void Insert_QuantidadePorcoesNegativa_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"Nome", -1, 2, SetUpIngredientes(), "modo");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Quantidade de porcoes invalida"));
        }

        // Caso quantidade de calorias não seja numero
        // Retorna 400 - Bad Request "Quantidade de calorias inválida"

        // Caso quantidade de calorias seja menor que zero
        // Retorna 400 - Bad Request "Quantidade de calorias inválida"
        [Test]
        public void Insert_QuantidadeCaloriasNegativa_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"Nome", 1, -2, SetUpIngredientes(), "modo");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Quantidade de calorias invalida"));
        }

        // Caso não exista modo de preparo
        // Retorna 400 - Bad Request "Por favor, conte-nos como preparar o prato"
        [Test]
        public void Insert_SemModoPreparo_RetornaBadRequest()
        {
            TearDown();
            ReceitaAdapter receitaTeste = new ReceitaAdapter(0,"Nome", 1, 2, SetUpIngredientes(), "");
            var result = (BadRequestErrorMessageResult)controller.Post(receitaTeste);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Receita deve ter modo de preparo"));
        }

        // Caso haja um erro na formação do body
        // Receita == null
        [Test]
        public void Insert_ReceitaNull_RetornaBadRequest()
        {
            TearDown();
            var result = (BadRequestErrorMessageResult)controller.Post(null);

            Assert.IsInstanceOf(typeof(BadRequestErrorMessageResult), result);
            Assert.IsTrue(result.Message.Contains("Dados Invalidos no Corpo da Requisicao"));
        }

        /************************************************************************************
        * GET /receitas/ingredientes (devolve todos os ingredientes utilizados em receitas) *
        ************************************************************************************/

        // Condição normal 1 - Apenas 1 receita
        // Retorna todos os ingredientes que são utilizados em receitas
        [Test]
        public void GetIngredientesUtilizados_Caso1_RetornaLista()
        {
            SetupGetIngredientesUtilizados();

            var result = (OkNegotiatedContentResult<List<Ingrediente>>)controller.GetIngredientesUtilizados();

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Ingrediente>>), result);
            Assert.AreEqual(3, result.Content.Count);
        }

        // Condição normal 2 - Receitas que repetem ingredientes
        // Retorna apenas 1 de cada ingrediente
        [Test]
        public void GetIngredientesUtilizados_Caso2_RetornaLista()
        {
            //Utiliza um setup que repete ingredientes nas receitas
            SetupGetReceitasIngredienteID();

            var result = (OkNegotiatedContentResult<List<Ingrediente>>)controller.GetIngredientesUtilizados();

            Assert.IsInstanceOf(typeof(OkNegotiatedContentResult<List<Ingrediente>>), result);
            Assert.AreEqual(3, result.Content.Count);
        }


        // Caso não existam ingredientes utilizados
        // Retorna 404 - Not Found
        [Test]
        public void GetIngredientesUtilizados_NenhumaReceita_RetornaNotFound()
        {
            TearDown();

            var result = controller.GetIngredientesUtilizados();

            Assert.IsInstanceOf(typeof(NotFoundResult), result);
        }
    }
}
