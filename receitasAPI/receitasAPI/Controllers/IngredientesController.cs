﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using receitas.Models;
using receitas.FakeDatabase;
using System.Web.Http;
using System.Web.Http.Cors;

namespace receitasAPI.Controllers
{
    
    public class IngredientesController : ApiController
    {
        public static void Initialize()
        {
            IngredienteTable.Instance.Clear();
            IngredienteTable.Instance.Insert(new Ingrediente(1, "Arroz"));
            IngredienteTable.Instance.Insert(new Ingrediente(2, "Feijao"));
            IngredienteTable.Instance.Insert(new Ingrediente(3, "Agua"));
            IngredienteTable.Instance.Insert(new Ingrediente(4, "Farinha"));
            IngredienteTable.Instance.Insert(new Ingrediente(5, "Ovo"));
            IngredienteTable.Instance.Insert(new Ingrediente(6, "Oleo"));
        }

        // GET /receitas/{id}/ingredientes (devolve os ingredientes de uma receita)
        [Route("api/receitas/{id}/ingredientes")]
        public IHttpActionResult GetIngredientesDaReceita(int id)
        {
            if (id < 0) return BadRequest("Id invalido");

            List<Ingrediente> result = ItemReceitaTable.Instance.GetIngredientesDeReceitaPorId(id);

            if (result == null) return NotFound();
            if (result.Count == 0) return NotFound();

            return Ok(result);
        }

        // GET: api/ingredientes
        // GET /ingredientes
        public IHttpActionResult Get()
        {
            List<Ingrediente> result = IngredienteTable.Instance.Get();

            if (result == null) return NotFound();
            if (result.Count == 0) return NotFound();

            return Ok(result);
        }

        

        /*
        // POST: api/Ingredientes
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Ingredientes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Ingredientes/5
        public void Delete(int id)
        {
        }
        */
    }
}
