﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using receitas.Models;
using receitas.FakeDatabase;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using receitasAPI.Models;

namespace receitasAPI.Controllers
{
    public class ReceitasController : ApiController
    {

        // GET: api/Receitas
        // GET /receitas (devolve todas as receitas em memória)
        public IHttpActionResult Get()
        {
            try
            {
                List<Receita> result = ReceitaTable.Instance.Get();

                if (result.Count == 0) return NotFound();

                return Ok(result);

            } catch (Exception ex)
            {
                return InternalServerError(ex);
            }
            
        }

        // GET: api/Receitas/{id}
        // GET /receitas/{id} (devolve uma receita em memória por id)
        [Route("api/receitas/{id}")]
        public IHttpActionResult GetReceita(int id)
        {
            if (id < 0) return BadRequest("Id invalido");

            var result = ReceitaTable.Instance.Get(id);

            if (result == null) return NotFound();

            return Ok(result);
        }

        // POST: api/receita
        // POST /receita (inicializa uma receita)  
        [Route("api/receita")]
        public IHttpActionResult Post([FromBody]ReceitaAdapter payload)
        {
            if (payload == null) return BadRequest("Dados Invalidos no Corpo da Requisicao");

            if (payload._ingredientes == null) return BadRequest("Dados Invalidos no Corpo da Requisicao");

            List<Ingrediente> listaIngredientesPayload = new List<Ingrediente>();
            foreach(IngredienteAdapter ing in payload._ingredientes)
            {
                listaIngredientesPayload.Add(new Ingrediente(ing._id, ing._nome));
            }
            

            Receita value = new Receita(payload._nome, payload._porcoes,
                payload._calorias, listaIngredientesPayload, payload._modoDePreparo);
            
            if (value == null) return BadRequest("Dados Invalidos no Corpo da Requisicao");

            var erros = validaReceita(value);

            if (String.IsNullOrEmpty(erros))
            {
                ReceitaTable.Instance.Insert(value);
                return Created("", value.Id.ToString());
            }

            return BadRequest(erros);

        }

        // GET /receitas/ingredientes/{id} (devolve receitas que contenham o ingrediente definido por id)
        [Route("api/receitas/ingredientes/{id}")]
        public IHttpActionResult GetReceitasComIngrediente(int id)
        {
            if (id < 0) return BadRequest("Id invalido");
            List<Receita> result = ItemReceitaTable.Instance.GetReceitasCom(new Ingrediente(id, ""));

            if (result == null) return NotFound();
            if (result.Count == 0) return NotFound();

            return Ok(result);
        }

        [Route("api/receitas/ingredientes")]
        public IHttpActionResult GetIngredientesUtilizados()
        {
            List<Ingrediente> result = ItemReceitaTable.Instance.GetIngredientesUtilizados();

            if (result == null) return NotFound();
            if (result.Count == 0) return NotFound();

            return Ok(result);
        }

        private String validaReceita(Receita receita)
        {
            String retorno = "";

            if (receita.Ingredientes == null) retorno += "Receita deve conter ingredientes\n";
            else if (receita.Ingredientes.Count == 0) retorno += "Receita deve conter ingredientes\n";
            if (String.IsNullOrEmpty(receita.Nome)
                || String.IsNullOrWhiteSpace(receita.Nome)) retorno += "Receita deve ter um nome\n";
            if (String.IsNullOrEmpty(receita.ModoDePreparo)
                || String.IsNullOrWhiteSpace(receita.ModoDePreparo)) retorno += "Receita deve ter modo de preparo\n";

            Regex regex = new Regex(@"^[0-9]*$");

            if (regex.IsMatch(receita.Nome)) retorno += "Nome de receita invalido\n";

            if (receita.Porcoes < 0) retorno += "Quantidade de porcoes invalida\n";
            if (receita.Calorias < 0) retorno += "Quantidade de calorias invalida\n";

            return retorno;
        }
        /*
        // PUT: api/Receitas/5
        public void Put(int id, [FromBody]string value)
        {
        }
        */

        /*
        // DELETE: api/Receitas/5
        public void Delete(int id)
        {
        }
        */
    }
}
