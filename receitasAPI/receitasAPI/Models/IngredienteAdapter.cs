﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace receitasAPI.Models
{
    public class IngredienteAdapter
    {
        public int _id {get; set;}
        public string _nome { get; set; }

        public IngredienteAdapter(int id, string nome)
        {
            _id = id;
            _nome = nome;
        }
    }
}