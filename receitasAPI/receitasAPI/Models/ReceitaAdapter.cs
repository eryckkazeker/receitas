﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace receitasAPI.Models
{
    public class ReceitaAdapter
    {
        public int _id { get; set; }
        public string _nome { get; set; }
        public int _porcoes { get; set; }
        public int _calorias { get; set; }
        public List<IngredienteAdapter> _ingredientes { get; set; }
        public string _modoDePreparo { get; set; }

        public ReceitaAdapter(int _id, string _nome, int _porcoes, int _calorias, List<IngredienteAdapter> _ingredientes, string _modoDePreparo)
        {
            this._id = _id;
            this._nome = _nome;
            this._porcoes = _porcoes;
            this._calorias = _calorias;
            this._ingredientes = _ingredientes;
            this._modoDePreparo = _modoDePreparo;
        }
    }
}