class CadastroReceitaController {
    constructor() {
        this._$ = document.querySelector.bind(document);

        this._ingredienteService = new IngredienteService();
        this._receitaService = new ReceitaService();

        this._ingredientesSelecionados = [];

        this._nomeReceitaInput = this._$('#nomeReceita');
        this._rendimentoInput = this._$('#rendimento');
        this._caloriasInput = this._$('#calorias');
        this._modoPreparoInput = this._$('#modoPreparo');
        
        this._mensagemView = new MensagemView(this._$('#mensagem'));
        this._listaIngredientesView = new ListaIngredientesView(this._$('#listaIngredientes'));
        this._ingredientesSelecionadosView = new IngredientesSelecionadosView(this._$('#ingredientesSelecionados'));
        this._inicializaListaIngredientes();
    }

    _inicializaListaIngredientes() {
        this._listaIngredientes = this._ingredienteService.obterListaDeIngredientes()
        .then(ingredientes => {
            this._listaIngredientes = ingredientes;
            this._listaIngredientesView.update(ingredientes);
        });
    }

    adicionaIngrediente() {
        let opcao = this._$('#opcoesIngredientes');
        let ingredienteSelecionado = this._listaIngredientes[opcao.selectedIndex];

        if(this._ingredientesSelecionados.indexOf(ingredienteSelecionado) < 0)
        {
            this._ingredientesSelecionados = this._ingredientesSelecionados.concat(ingredienteSelecionado);
        }

        this._ingredientesSelecionadosView.update(this._ingredientesSelecionados);
    }

    enviaReceita(event) {
        event.preventDefault();
        this._receitaService.cadastrarReceita(this._montaReceita())
        .then(mensagem => {
            this._mensagemView.update(mensagem);
            this._limpaFormulario();
        }).catch(erro => this._mensagemView.update(erro));
    }

    _montaReceita() {
        return new Receita(
            0,
            this._nomeReceitaInput.value,
            parseInt(this._rendimentoInput.value),
            parseInt(this._caloriasInput.value),
            this._ingredientesSelecionados,
            this._modoPreparoInput.value
        );
    }

    _limpaFormulario() {
        this._caloriasInput.value = '';
        this._ingredientesSelecionados = [];
        this._modoPreparoInput.value = '';
        this._nomeReceitaInput.value = '';
        this._rendimentoInput.value = '';
        this._ingredientesSelecionadosView.update(this._ingredientesSelecionados);
    }

}