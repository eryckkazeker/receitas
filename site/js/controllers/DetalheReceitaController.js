class DetalheReceitaController {
    constructor(idReceita) {
        let $ = document.querySelector.bind(document);

        this._idReceita = idReceita;

        this._view = new DetalheReceitaView($('#detalheReceita'));
        this._service = new ReceitaService();

        this._inicializa();
    }

    _inicializa() {
        this._receita = this._service.obterReceitaPorId(this._idReceita)
            .then(receita => {
                this._view.update(receita)
            });

    }
}