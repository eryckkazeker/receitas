class ListaReceitasController {
    constructor() {
        let $ = document.querySelector.bind(document);
        
        this._listaReceitas = [];

        this._view = new ListaReceitasView($('#listaReceitas'));

        this._service = new ReceitaService();
    }

    inicializa() {
        this._buscaTodasAsReceitas();
    }

    _buscaTodasAsReceitas() {
        this._listaReceitas = this._service.obterListaDeReceitas()
        .then(lista => {
            this._view.update(lista)
        });

    }
}