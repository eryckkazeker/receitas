class Ingrediente {
    constructor(id, nome) {

        this._id = id;
        this._nome = nome;
    }

    get id() {
        return this._id;
    }

    set nome(nome) {
        this._nome = nome;
    }
    get nome() {
        return this._nome;
    }
}