class Receita {
    constructor(id, nome, porcoes, calorias, ingredientes, modoDePreparo) {

        this._id = id;
        this._nome = nome;
        this._porcoes = porcoes;
        this._calorias = calorias;
        this._modoDePreparo = modoDePreparo;
        this._ingredientes = ingredientes;
    }

    get id() {
        return this._id;
    }

    set nome(nome) {
        this._nome = nome;
    }
    get nome() {
        return this._nome;
    }

    set porcoes(porcoes) {
        this._porcoes = porcoes;
    }
    get porcoes() {
        return this._porcoes;
    }

    set calorias(calorias) {
        this._calorias = calorias;
    }
    get calorias() {
        return this._calorias;
    }

    set modoDePreparo(modoDePreparo) {
        this._modoDePreparo = modoDePreparo;
    }
    get modoDePreparo() {
        return this._modoDePreparo;
    }

    get ingredientes() {
        return [].concat(this._ingredientes);
    }
}