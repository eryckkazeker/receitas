class HttpService {

    constructor() {
        this._host = new Util().host;
    }

    get(url) {
        return fetch(this._host+url)
            .then(res => this._handleErrors(res))
            .then(res => res.json());
    }

    post(url, body) {
        return fetch(this._host+url, {
            headers: { 'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin':'*'},
            method: 'post',
            mode: 'cors',
            body: JSON.stringify(body)
        })
        .then(res => this._handleErrors(res))
        .then(res => res.json());
    }

    _handleErrors(res) {
        if(res.ok) return res;
        else if(res.created) return res;
        else throw new Error(res.statusText);
    }
}