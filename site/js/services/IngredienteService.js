class IngredienteService {
    constructor() {
        this.http = new HttpService();
    }

    obterListaDeIngredientes() {
        return new Promise((resolve,reject) => {
            this.http.get('/ingredientes')
            .then(ingredientes => {
                resolve(ingredientes.map(objeto => new Ingrediente(objeto.Id,objeto.Nome)));
            })
            .catch(erro => {
                console.log(erro);
                reject('Não foi possível obter a lista de ingredientes');
            });
        });
        
    }
}