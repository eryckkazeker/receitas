class ReceitaService {
    constructor() {
        this.http = new HttpService();
    }
    obterListaDeReceitas() {
        return new Promise((resolve,reject) => {
            this.http.get('/receitas')
            .then(receitas => {
                resolve(receitas.map(objeto => new Receita(objeto.Id,objeto.Nome)));
            })
            .catch(erro => {
                console.log(erro);
                reject('Não foi possível obter a lista de receitas');
            });
        });
        
    }

    obterReceitaPorId(id) {
        return new Promise((resolve,reject) => {
            this.http.get(`/receitas/${id}`)
            .then(objeto => {
                resolve(new Receita(objeto.Id,objeto.Nome,objeto.Porcoes,
                    objeto.Calorias, objeto.Ingredientes, objeto.ModoDePreparo));
            })
            .catch(erro => {
                console.log(erro);
                reject('Não foi possível encontrar a receita desejada');
            });
        });
    }

    obterReceitasComOIngrediente(idIngrediente) {
        return new Promise((resolve,reject) => {
            this.http.get(`/ingredientes/${idIngrediente}`)
            .then(receitas => {
                resolve(receitas.map(objeto => new Receita(objeto.id,objeto.nome,objeto.porcoes,
                    objeto.calorias, objeto.ingredientes, objeto.modoDePreparo)));
            })
            .catch(erro => {
                console.log(erro);
                reject('Não foi possível encontrar as receitas');
            });
        });
    }

    cadastrarReceita(receita) {
        return new Promise((resolve,reject) => {
            this.http.post(`/receita`,receita)
            .then(() => {
                resolve("Receita cadastrada com sucesso!");
            })
            .catch(erro => {
                console.log(erro);
                reject('Não foi possível cadastrar a receita');
            });
        });
    }
}