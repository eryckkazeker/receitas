class DetalheReceitaView extends View{

    constructor(elemento) {
        super(elemento);
    }

    template(receita) {
        return `<h1 class="text-center">${receita.nome}</h1>
                <h2 class="text-left">Ingredientes:</h2>
                <ul class="list-group">
                    
                        ${receita.ingredientes.map(ingrediente=>
                            `<li class ="list-group-item">${ingrediente.Nome}</li>`).join('')}
                    
                </ul>
                <h2>Modo de Preparo:</h2>
                <p>${receita.modoDePreparo}</p>
                <h2>Outras informações:</h2>
                <h3>Quantidade de Porções: ${receita.porcoes}</h3>
                <h3>Calorias: ${receita.calorias}</h3>`;
    }

}