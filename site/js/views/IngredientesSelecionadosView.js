class IngredientesSelecionadosView extends View{

    constructor(elemento) {
        super(elemento);
    }

    template(listaIngredientes) {
        
        return `<label>Ingredientes Selecionados</label>
                <ul class="list-group">
                    
                        ${listaIngredientes.map(ingrediente=>
                            `<li class ="list-group-item">${ingrediente.nome}</li>`).join('')}
                    
                </ul>`;
    }

}