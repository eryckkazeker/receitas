class ListaIngredientesView extends View{

    constructor(elemento) {
        super(elemento);
    }

    template(listaIngredientes) {
        return `<label>Ingredientes:</label>
                <select id="opcoesIngredientes" class="list-group">
                    ${listaIngredientes.map(ingrediente => 
                    `<option value=${ingrediente}>${ingrediente.nome}</option>`).join('')}
                </select>
                <button class="btn btn-primary form-group text-center" 
                type="button"
                onClick="controller.adicionaIngrediente()">Selecionar</button>`;
    }

}