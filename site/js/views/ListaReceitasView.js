class ListaReceitasView extends View{

    constructor(elemento) {
        super(elemento);
    }

    template(listaReceitas) {
        return `<table class="table table-hover table-bordered text-center">
            <tbody>
                ${listaReceitas.map(receita =>
                    `<tr>
                        <td onClick="window.location.href = './detalhes_receita.html?id=${receita.id}'">${receita.nome}</td>
                    </tr>`
                ).join('')}
            </tbody>
        </table>`;
    }

}