class MensagemView extends View{

    constructor(elemento) {
        super(elemento);
    }

    template(texto) {
        return texto ? `<p class="alert alert-info">${texto}</p>` : '<p></p>';
    }
}